package com.rufino.camel;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.processor.aggregate.AggregationStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import javax.sql.DataSource;

@Configuration
public class Router extends RouteBuilder {
    @Autowired
    Environment environment;

    @Autowired
    BuildSQLProcessor buildSQLProcessor;

    @Override
    public void configure() throws Exception {
        from("{{startRoute}}")
                .log("Timer Invoked and the body" + environment.getProperty("message"))
                .to("sql: select * from ITEMS?dataSource=#postgres")
                .split(body())
                    .process(buildSQLProcessor)
                .to("jdbc:mysql");


    }
}
