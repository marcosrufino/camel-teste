package com.rufino.camel;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class Item {

    //private String transactionType;
    private Integer item_id;

    private String sku;

    private String itemDescription;

    private BigDecimal price;






}
