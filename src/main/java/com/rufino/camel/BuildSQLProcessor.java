package com.rufino.camel;

import lombok.extern.slf4j.Slf4j;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedCaseInsensitiveMap;

import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Component
public class BuildSQLProcessor implements Processor {

    @Override
    public void process(Exchange exchange) throws Exception {
        Map<String, Object> params = (Map<String, Object>) exchange.getIn().getBody();
        String tableName ="ITEMS";
        StringBuilder query = new StringBuilder();
            query.append("INSERT INTO "+tableName+ "(SKU, ITEM_DESCRIPTION,PRICE) VALUES ('");
            query.append(params.get("sku")+"','"+params.get("item_description")+"',"+params.get("price")+")");
        log.info("Final Query is : " + query);
        exchange.getIn().setBody(query.toString());
    }
}
